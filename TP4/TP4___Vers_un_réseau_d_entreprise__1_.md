# TP4 : Vers un réseau d'entreprise



# 0. Prérequis



## Checklist VM Linux



# I. Dumb switch

## 1. Topologie 1

![Topologie 1](./pics/topo1.png)

## 2. Adressage topologie 1

| Node  | IP            |
|-------|---------------|
| `pc1` | `10.1.1.1/24` |
| `pc2` | `10.1.1.2/24` |

## 3. Setup topologie 1

🌞 **Commençons simple**

- définissez les IPs statiques sur les deux VPCS
- `ping` un VPCS depuis l'autre

        PC1> ip 10.1.1.1/24
        Checking for duplicate address...
    
        PC2> ip 10.1.1.2/24
        Checking for duplicate address...
        PC2> ping 10.1.1.1 -c 2

        84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=8.108 ms
        84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=9.151 ms

# II. VLAN


## 1. Topologie 2

![Topologie 2](./pics/topo2.png)

## 2. Adressage topologie 2

| Node  | IP            | VLAN |
|-------|---------------|------|
| `pc1` | `10.1.1.1/24` | 10   |
| `pc2` | `10.1.1.2/24` | 10   |
| `pc3` | `10.1.1.3/24` | 20   |

### 3. Setup topologie 2

🌞 **Adressage**

- définissez les IPs statiques sur tous les VPCS

        PC1> ip 10.1.1.1/24
        Checking for duplicate address...
        PC1 : 10.1.1.1 255.255.255.0
    
        PC2> ip 10.1.1.2/24
        Checking for duplicate address...
        PC2 : 10.1.1.2 255.255.255.0
        
        PC3> ip 10.1.1.3/24
        Checking for duplicate address...
        PC3 : 10.1.1.3 255.255.255.0

        
          
- vérifiez avec des `ping` que tout le monde se ping

        PC3> ping 10.1.1.1 -c 1

        84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=6.196 ms

        PC3> ping 10.1.1.2 -c 1

        84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=7.005 ms
        
        PC2> ping 10.1.1.1 -c 2

        84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=8.108 ms
        84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=9.151 ms
        PC2> ping 10.1.1.3 -c 1

        84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=5.680 ms

        PC1> ping 10.1.1.2 -c 1

        84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=6.138 ms

        PC1> ping 10.1.1.3 -c 1

        84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=6.400 ms

🌞 **Configuration des VLANs**

VLAN:
    Switch>enable
    Switch#conf t
    Switch(config)#vlan 10
    Switch(config-vlan)#
    Switch(config-vlan)#name vlan10
    Switch(config-vlan)#
    Switch(config-vlan)#exit
    Switch(config)#
    Switch(config)#vlan 20
    Switch(config-vlan)#
    Switch(config-vlan)#name vlan20
    Switch(config-vlan)#
    Switch(config-vlan)#exit
    Switch(config)#

ACCESS:
    Switch(config)#interface range gigabitEthernet 0/0-1
    Switch(config-if-range)#switchport mode access
    Switch(config-if-range)#switchport access vlan 10
    Switch(config-if-range)#no shutdown
    Switch(config-if-range)#exit

TRUNK:   
    Switch(config)#interface gigabitEthernet 0/2
    Switch(config-if)#switchport mode access
    Switch(config-if)#switchport access vlan 20
    Switch(config-if)#no shutdown
    Switch(config-if)#exit
    
    
🌞 **Vérif**

- `pc1` et `pc2` doivent toujours pouvoir se ping
    
        PC1> ping 10.1.1.2 -c 1

        84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=6.108 ms

        PC1> ping 10.1.1.3 -c 1

        host (10.1.1.3) not reachable
        
        PC2> ping 10.1.1.3 -c 1

        host (10.1.1.3) not reachable

        PC2> ping 10.1.1.2 -c 1

        10.1.1.2 icmp_seq=1 ttl=64 time=0.001 ms
    
- `pc3` ne ping plus personne


        PC3> ping 10.1.1.1 -c1

        host (10.1.1.1) not reachable

        PC3> ping 10.1.1.2 -c1

        host (10.1.1.2) not reachable

# III. Routing
.

## 1. Topologie 3

![Topologie 3](./pics/topo3.png)

## 2. Adressage topologie 3

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `admins`  | `10.2.2.0/24` | 12           |
| `servers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`       | `admins`        | `servers`       |
|--------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`  | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`  | `10.1.1.2/24`   | x               | x               |
| `adm1.admins.tp4`  | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4` | x               | x               | `10.3.3.1/24`   |
| `r1`               | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

## 3. Setup topologie 3

🖥️ VM `web1.servers.tp4`, déroulez la [Checklist VM Linux](#checklist-vm-linux) dessus

🌞 **Adressage**

- définissez les IPs statiques sur toutes les machines **sauf le *routeur***

        
        PC1> ip 10.1.1.1/24
        Checking for duplicate address...
        PC1 : 10.1.1.1 255.255.255.0
    
        PC2> ip 10.1.1.2/24
        Checking for duplicate address...
        PC2 : 10.1.1.2 255.255.255.0
        
        adm1> ip 10.2.2.1/24
        Checking for duplicate address...
        adm1 : 10.2.2.1 255.255.255.0

🌞 **Configuration des VLANs**

VLAN:
    Switch(config)#vlan 11
    Switch(config-vlan)#
    Switch(config-vlan)#nam
    Switch(config-vlan)#name clients
    Switch(config-vlan)#
    Switch(config-vlan)#exit
    Switch(config)#
    Switch(config)#vlan 12
    Switch(config-vlan)#
    Switch(config-vlan)#name admins
    Switch(config-vlan)#
    Switch(config-vlan)#exit
    Switch(config)#
    Switch(config)#vlan 13
    Switch(config-vlan)#
    Switch(config-vlan)#name servers
    Switch(config-vlan)#
    Switch(config-vlan)#exit

ACCESS:    
    Switch(config)#interface range gigabitEthernet 0/0 -1
    Switch(config-if-range)#switchport mode access
    Switch(config-if-range)#switchport access vlan 11
    Switch(config-if-range)#no shutdown
    Switch(config-if-range)#
    Switch(config-if-range)#exit

    Switch(config)#interface gigabitEthernet 0/2
    Switch(config-if)#switchport mode acc
    Switch(config-if)#switchport access vlan 12
    Switch(config-if)#no shutdown
    Switch(config-if)#exit
    
    Switch(config)#interface gigabitEthernet 0/3
    Switch(config-if)#switchport mode access
    Switch(config-if)#switchport access vlan 13
    Switch(config-if)#no sh
    Switch(config-if)#
    Switch(config-if)#exit

TRUNK:    
    Switch(config)#interface gigabitEthernet 1/0
    Switch(config-if)#switchport trunk encapsulation dot1q
    Switch(config-if)#switchport mode trunk
    Switch(config-if)#switchport trunk allowed vlan add 11,12,13

```

🌞 **Config du *routeur***

- attribuez ses IPs au *routeur*
  - 3 sous-interfaces, chacune avec son IP et un VLAN associé


        R1#enable
        R1#conf t
        R1(config)#interface fastEthernet 0/0.11
        R1(config-subif)#encapsulation dot1Q 11
        R1(config-subif)#ip address 10.1.1.254 255.255.255.0
        R1(config-subif)#exit

        R1(config)#interface fastEthernet 0/0.12
        R1(config-subif)#encapsulation dot1Q 12
        R1(config-subif)#ip address 10.2.2.254 255.255.255.0
        R1(config-subif)#no sh
        R1(config-subif)#exit

        R1(config)#interface fastEthernet 0/0.13
        R1(config-subif)#encapsulation dot1Q 13
        R1(config-subif)#ip address 10.3.3.254 255.255.255.0
        R1(config-subif)#exit

        R1(config)#interface fastEthernet 0/0
        R1(config-if)#no sh
        R1(config-if)#no shutdown
        R1(config-if)#exit
        R1(config)#
        *Mar  1 00:05:38.599: %LINK-3-UPDOWN: Interface FastEthernet0/0, changed state to up
        [...]
        
🌞 **Vérif**

- tout le monde doit pouvoir ping le routeur sur l'IP qui est dans son réseau

PC1:
        PC1> ping 10.1.1.254 -c 1

        84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=13.639 ms

PC2:
        PC2> ping 10.1.1.254 -c 1

        84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=15.786 ms    

ADM1:
        adm1> ping 10.2.2.254 -c 1

        84 bytes from 10.2.2.254 icmp_seq=1 ttl=255 time=15.033 ms

WEB:        
        [david@web1 ~]$ ping 10.3.3.254 -c 1
        PING 10.3.3.254 (10.3.3.254) 56(84) bytes of data.
        64 bytes from 10.3.3.254: icmp_seq=1 ttl=255 time=20.4 ms

        --- 10.3.3.254 ping statistics ---
        1 packets transmitted, 1 received, 0% packet loss, time 0ms
        rtt min/avg/max/mdev = 20.418/20.418/20.418/0.000 ms
        [david@web1 ~]$
        
        
- en ajoutant une route vers les réseaux, ils peuvent se ping entre eux

        Switch(config)#ip route 10.1.1.0 255.255.255.0 10.1.1.254
        Switch(config)#
        Switch(config)#ip route 10.3.3.0 255.255.255.0 10.3.3.254
        Switch(config)#
        Switch(config)#ip route 10.2.2.0 255.255.255.0 10.2.2.254
        
  - ajoutez une route par défaut sur les VPCS

PC1:
        PC1> ip 10.1.1.1/24 10.1.1.254
        Checking for duplicate address...
        PC1 : 10.1.1.1 255.255.255.0 gateway 10.1.1.254

PC2:
        PC2> ip 10.1.1.2/24 10.1.1.254
        Checking for duplicate address...
        PC2 : 10.1.1.2 255.255.255.0 gateway 10.1.1.254
ADM1:        
        adm1> ip 10.2.2.1/24 10.2.2.254
        Checking for duplicate address...
        adm1 : 10.2.2.1 255.255.255.0 gateway 10.2.2.254

  - ajoutez une route par défaut sur la machine virtuelle

        [david@web1 ~]$ sudo nmcli connection modify gns3 ipv4.gateway 10.3.3.254
        [sudo] password for david:
        [david@web1 ~]$ sudo nmcli connection reload
        [david@web1 ~]$ sudo nmcli connection down gns3
        Connection 'gns3' successfully deactivated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/4)
        [david@web1 ~]$ sudo nmcli connection up gns3
        Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/8)
        [david@web1 ~]$

  - testez des `ping` entre les réseaux

        [david@web1 ~]$ ping 10.1.1.1 -c 1
        PING 10.1.1.1 (10.1.1.1) 56(84) bytes of data.
        64 bytes from 10.1.1.1: icmp_seq=1 ttl=63 time=43.8 ms

        --- 10.1.1.1 ping statistics ---
        1 packets transmitted, 1 received, 0% packet loss, time 0ms
        rtt min/avg/max/mdev = 43.846/43.846/43.846/0.000 ms
        [david@web1 ~]$ ping 10.2.2.1 -c 1
        PING 10.2.2.1 (10.2.2.1) 56(84) bytes of data.
        64 bytes from 10.2.2.1: icmp_seq=1 ttl=63 time=34.6 ms

        --- 10.2.2.1 ping statistics ---
        1 packets transmitted, 1 received, 0% packet loss, time 0ms
        rtt min/avg/max/mdev = 34.578/34.578/34.578/0.000 ms
        [david@web1 ~]$
        

        PC1> ping 10.2.2.1 -c 1

        84 bytes from 10.2.2.1 icmp_seq=1 ttl=63 time=32.810 ms

        PC1> ping 10.3.3.1 -c 1

        84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=34.798 ms
        
        PC3> ping 10.1.1.1 -c 1

        84 bytes from 10.1.1.1 icmp_seq=1 ttl=63 time=21.441 ms

        PC3> ping 10.3.3.1 -c 1

        84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=20.401 ms        

# IV. NAT

![Yellow cable](./pics/yellow-cable.png)

## 1. Topologie 4

![Topologie 3](./pics/topo4.png)

## 2. Adressage topologie 4

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `servers` | `10.2.2.0/24` | 12           |
| `routers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`       | `admins`        | `servers`       |
|--------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`  | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`  | `10.1.1.2/24`   | x               | x               |
| `adm1.admins.tp4`  | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4` | x               | x               | `10.3.3.1/24`   |
| `r1`               | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

## 3. Setup topologie 4

🌞 **Ajoutez le noeud Cloud à la topo**

- branchez à `eth1` côté Cloud
- côté routeur, il faudra récupérer un IP en DHCP (voir [le mémo Cisco](../../cours/memo/memo_cisco.md))
- vous devriez pouvoir `ping 1.1.1.1`


        R1(config)#interface fastEthernet 1/0
        R1(config-if)#ip address dhcp
        R1(config-if)#no shutdown
        R1(config-if)#exit
        R1(config)#
        *Mar  1 00:24:05.795: %LINK-3-UPDOWN: Interface FastEthernet1/0, changed state to up
        
        PC1> ping 1.1.1.1 -c 3

        84 bytes from 1.1.1.1 icmp_seq=1 ttl=55 time=42.214 ms
        84 bytes from 1.1.1.1 icmp_seq=2 ttl=55 time=45.909 ms
        84 bytes from 1.1.1.1 icmp_seq=3 ttl=55 time=39.264 ms

🌞 **Configurez le NAT**

- référez-vous [à la section NAT du mémo Cisco](../../cours/memo/memo_cisco.md#7-configuration-dun-nat-simple)

        R1(config)#interface fastEthernet 0/0.11
        R1(config-subif)#ip nat inside
        R1(config-subif)#exit

        R1(config)#interface fastEthernet 0/0.12
        R1(config-subif)#ip nat inside
        R1(config-subif)#exit

        R1(config)#interface fastEthernet 0/0.13
        R1(config-subif)#ip nat inside
        R1(config-subif)#exit

        R1(config)#interface fastEthernet 1/0
        R1(config-if)#ip nat outside
        R1(config-if)#exit

        R1(config)# access-list 1 permit any
        R1(config)# ip nat inside source list 1 interface fastEthernet 1/0 overload

🌞 **Test**

- ajoutez une route par défaut (si c'est pas déjà fait)
  - sur les VPCS
  - sur la machine Linux
- configurez l'utilisation d'un DNS
  - sur les VPCS

        PC1> ip dns 1.1.1.1
        PC2> ip dns 1.1.1.1
        adm1> ip dns 1.1.1.1
        
  - sur la machine Linux

        [david@web1 ~]$ sudo nmcli connection modify gns3 ipv4.dns 1.1.1.1
        [sudo] password for david:
        [david@web1 ~]$ sudo nmcli connection reload
        [david@web1 ~]$ sudo nmcli connection down gns3
        Connection 'gns3' successfully deactivated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/8)
        [david@web1 ~]$ sudo nmcli connection up gns3
        Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/9)
        [david@web1 ~]$
        
- vérifiez un `ping` vers un nom de domaine

        [david@web1 ~]$ ping google.com -c 2
        PING google.com (172.217.19.238) 56(84) bytes of data.
        64 bytes from par21s11-in-f14.1e100.net (172.217.19.238): icmp_seq=1 ttl=113 time=39.1 ms
        64 bytes from par21s11-in-f14.1e100.net (172.217.19.238): icmp_seq=2 ttl=113 time=35.10 ms

        --- google.com ping statistics ---
        2 packets transmitted, 2 received, 0% packet loss, time 1002ms
        rtt min/avg/max/mdev = 35.961/37.518/39.076/1.569 ms
        [david@web1 ~]$
        
        PC1> ping google.com -c 2
        google.com resolved to 216.58.204.110

        84 bytes from 216.58.204.110 icmp_seq=1 ttl=113 time=43.069 ms
        84 bytes from 216.58.204.110 icmp_seq=2 ttl=113 time=46.030 ms
        
        PC2> ping google.com -c 2
        google.com resolved to 172.217.19.238

        84 bytes from 172.217.19.238 icmp_seq=1 ttl=113 time=40.370 ms
        84 bytes from 172.217.19.238 icmp_seq=2 ttl=113 time=38.773 ms
        
        adm1> ping google.com -c 2
        google.com resolved to 216.58.209.238

        84 bytes from 216.58.209.238 icmp_seq=1 ttl=112 time=38.527 ms
        84 bytes from 216.58.209.238 icmp_seq=2 ttl=112 time=45.796 ms

# V. Add a building

On a acheté un nouveau bâtiment, faut tirer et configurer un nouveau switch jusque là-bas.

On va en profiter pour setup un serveur DHCP pour les clients qui s'y trouvent.

## 1. Topologie 5

![Topo 5](./pics/topo5.png)

## 2. Adressage topologie 5

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `servers` | `10.2.2.0/24` | 12           |
| `routers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node                | `clients`       | `admins`        | `servers`       |
|---------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`   | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`   | `10.1.1.2/24`   | x               | x               |
| `pc3.clients.tp4`   | DHCP            | x               | x               |
| `pc4.clients.tp4`   | DHCP            | x               | x               |
| `pc5.clients.tp4`   | DHCP            | x               | x               |
| `dhcp1.clients.tp4` | `10.1.1.253/24` | x               | x               |
| `adm1.admins.tp4`   | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4`  | x               | x               | `10.3.3.1/24`   |
| `r1`                | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

## 3. Setup topologie 5

Vous pouvez partir de la topologie 4. 

🌞  **Vous devez me rendre le `show running-config` de tous les équipements**

- de tous les équipements réseau
  - le routeur
  - les 3 switches

switch2:

    Switch#show running-config
    Building configuration...
    [...]
    spanning-tree mode pvst
    spanning-tree extend system-id
    !
    vlan internal allocation policy ascending
    !
    !
 
    interface GigabitEthernet0/0
     switchport access vlan 11
     switchport mode access
     media-type rj45
     negotiation auto
    !
    interface GigabitEthernet0/1
     switchport access vlan 11
     switchport mode access
     media-type rj45
     negotiation auto
    !
    interface GigabitEthernet0/2
     switchport access vlan 12
     switchport mode access
     media-type rj45
     negotiation auto
    !
    interface GigabitEthernet0/3
     switchport access vlan 13
     switchport mode access
     media-type rj45
     negotiation auto
    !
    interface GigabitEthernet1/0
     switchport trunk encapsulation dot1q
     switchport mode trunk
     media-type rj45
     negotiation auto
    [...]
    !
    ip route 10.1.1.0 255.255.255.0 10.1.1.254
    ip route 10.2.2.0 255.255.255.0 10.2.2.254
    ip route 10.3.3.0 255.255.255.0 10.3.3.254
    [...]
    end

switch 1:

    Switch#show running-config
    Building configuration...
    [...]
    interface GigabitEthernet0/0
     switchport trunk encapsulation dot1q
     switchport mode trunk
     media-type rj45
     negotiation auto
    !
    interface GigabitEthernet0/1
     switchport trunk encapsulation dot1q
     switchport mode trunk
     media-type rj45
     negotiation auto
    !
    interface GigabitEthernet0/2
     switchport trunk encapsulation dot1q
     switchport mode trunk
     media-type rj45
     negotiation auto
    !
    [...]
    ip route 10.1.1.0 255.255.255.0 10.1.1.254
    ip route 10.2.2.0 255.255.255.0 10.2.2.254
    ip route 10.3.3.0 255.255.255.0 10.3.3.254
    !

    [...]
    
    switch 3:


    Switch#sh running-config
    Building configuration...
    [...]
    interface GigabitEthernet0/0
     switchport access vlan 11
     switchport mode access
     media-type rj45
     negotiation auto
    !
    interface GigabitEthernet0/1
     switchport access vlan 11
     switchport mode access
     media-type rj45
     negotiation auto
    !
    interface GigabitEthernet0/2
     switchport access vlan 11
     switchport mode access
     media-type rj45
     negotiation auto
    !
    interface GigabitEthernet0/3
     switchport access vlan 11
     switchport mode access
     media-type rj45
     negotiation auto
    !
    interface GigabitEthernet1/0
     switchport trunk encapsulation dot1q
     switchport mode trunk
     media-type rj45
     negotiation auto
    !
    [...]
    !
    ip route 10.1.1.0 255.255.255.0 10.1.1.254
    ip route 10.2.2.0 255.255.255.0 10.2.2.254
    ip route 10.3.3.0 255.255.255.0 10.3.3.254
    [...]

Routeur:
    
    R1(config)#do sh run
    Building configuration...
    [...]
    interface FastEthernet0/0
     no ip address
     ip virtual-reassembly
     duplex auto
     speed auto
    !
    interface FastEthernet0/0.11
     encapsulation dot1Q 11
     ip address 10.1.1.254 255.255.255.0
     ip nat inside
     ip virtual-reassembly
    !
    interface FastEthernet0/0.12
     encapsulation dot1Q 12
     ip address 10.2.2.254 255.255.255.0
     ip nat inside
     ip virtual-reassembly
    !
    interface FastEthernet0/0.13
     encapsulation dot1Q 13
     ip address 10.3.3.254 255.255.255.0
     ip nat inside
     ip virtual-reassembly
    !
    interface FastEthernet1/0
     ip address dhcp
     ip nat outside
     ip virtual-reassembly
     duplex auto
     speed auto
    !
    [...]
    !
    no ip http server
    ip forward-protocol nd
    !
    !
    ip nat inside source list 1 interface FastEthernet1/0 overload
    !
    access-list 1 permit any
    [...]


🖥️ **VM `dhcp1.client1.tp4`**, déroulez la [Checklist VM Linux](#checklist-vm-linux) dessus

🌞  **Mettre en place un serveur DHCP dans le nouveau bâtiment**


        [david@dhcp1 ~]$ sudo vim /etc/dhcp/dhcpd.conf
        # specify domain name
        option domain-name   "clients.tp4" ;
        # specify DNS server's hostname or IP address
        option domain-name-servers 1.1.1.1 ;
        # default lease time
        default-lease-time 600;
        # max lease time
        max-lease-time 7200;
        # this DHCP server to be declared valid
        authoritative;
        # specify network address and subnetmask
        subnet 10.1.1.0 netmask 255.255.255.0 {
                # specify the range of lease IP address
                range dynamic-bootp 10.1.1.3 10.1.1.252;
                # specify broadcast address
                option broadcast-address 10.1.1.255;
                # specify gateway
                option routers 10.1.1.254;
                }
                
        david@dhcp1 ~]$ sudo systemctl start dhcpd.service
        [david@dhcp1 ~]$ sudo systemctl enable dhcpd.service
        Created symlink /etc/systemd/system/multi-user.target.wants/dhcpd.service → /usr/lib/systemd/system/dhcpd.service.
        [david@dhcp1 ~]$ sudo firewall-cmd --add-service=dhcp --permanent
    success
    [david@dhcp1 ~]$ sudo firewall-cmd --reload
    success

       
> Réutiliser les serveurs DHCP qu'on a monté dans les autres TPs.

🌞  **Vérification**

- un client récupère une IP en DHCP
- il peut ping le serveur Web
- il peut ping `8.8.8.8`
- il peut ping `google.com`

> Faites ça sur n'importe quel VPCS que vous venez d'ajouter : `pc3` ou `pc4` ou `pc5`.

    
PC3:

    PC3> ip dhcp
    DD

    ORA IP 10.1.1.3/24 GW 10.1.1.254
    
    PC3> ping 8.8.8.8 -c 2

    84 bytes from 8.8.8.8 icmp_seq=1 ttl=55 time=95.700 ms
    84 bytes from 8.8.8.8 icmp_seq=2 ttl=55 time=123.194 ms
    
    PC3> ping google.com -c 2
    google.com resolved to 142.250.179.110

    84 bytes from 142.250.179.110 icmp_seq=1 ttl=114 time=82.270 ms
    84 bytes from 142.250.179.110 icmp_seq=2 ttl=114 time=80.070 ms
    
    PC3> ping 10.3.3.1 -c 2

    84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=106.452 ms
    84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=180.470 ms
    
    PC3>



# Private VLAN

https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP4/PVLAN.png

## Prérequis à la configuration du Private-Vlan:

    Switch(config)#vtp mode transparent
    Setting device to VTP Transparent mode for VLANS.

Le principe est de crééer 3 zones. La zone principale (dite (primary qui contiendra une zone dans laquelle tous les hôtes pourront communiquer entre eux (dite communautaire), et une autre où chaque hôte sera isolé.

### Configuration zone communautaire: 

    Switch(config)#vlan 501
    Switch(config-vlan)#
    Switch(config-vlan)#private-vlan community
    Switch(config-vlan)#
    Switch(config-vlan)#vlan 500
    Switch(config-vlan)#private-vlan primary
    Switch(config-vlan)#private-vlan  association add 501

### COnfiguration des ports côté PC:

    Switch(config)#interface range gigabitEthernet 0/0 - 1
    Switch(config-if-range)#switchport mode private-vlan host
    Switch(config-if-range)#switchport private-vlan host-association 500 501

Vérifications:

    PC1> ping 10.1.1.2 -c 2

    84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=3.105 ms
    84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=4.202 ms

    PC2> ping 10.1.1.1 -c 1

    84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=13.388 ms


### Configuration du port côté Routeur:

Cette configuration en mode "promiscuous" est nécessaire pour que les PC puissent communiquer avec d'autres réseaux

    Switch(config)#interface gigabitEthernet 1/0
    Switch(config-if)#switchport mode private-vlan promiscuous
    Switch(config-if)#switchport private-vlan mapping  500 501
    Switch(config-if)#no shutdown
    Switch(config-if)#exit

### Configuration zone isolé: 

    Switch(config)#vlan 502
    Switch(config-vlan)#private-vlan isolated
    Switch(config-vlan)#vlan 500
    Switch(config-vlan)#private-vlan primary
    Switch(config-vlan)#private-vlan association add 502

### COnfiguration des ports côté PC:

    Switch(config)#interface range gigabitEthernet 0/2-3
    Switch(config-if-range)#switchport mode private-vlan host
    Switch(config-if-range)#switchport private-vlan host-association 500 502

### COnfiguration du port côté Routeur:

    Switch(config)#interface gigabitEthernet 1/0
    Switch(config-if)#switchport mode private-vlan promiscuous
    Switch(config-if)#switchport private-vlan mapping 500 502

Vérifications:

    PC3> ping 10.1.1.4

    host (10.1.1.4) not reachable

    PC4> ping 10.1.1.3 -c 2

    10.1.1.3 icmp_seq=1 timeout
    10.1.1.3 icmp_seq=2 timeout
