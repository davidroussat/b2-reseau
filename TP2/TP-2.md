# TP2 : On va router des trucs

# 1. Echange ARP

## Générer des requêtes ARP

Connection en ssh sur node1 

PS C:\WINDOWS\system32> ssh david@10.2.1.11
david@10.2.1.11's password:

    Activate the web console with: systemctl enable --    now cockpit.socket
    Last login: Mon Sep 20 16:17:52 2021 from 10.2.1.1
    [david@node1 ~]$
    
Connection en ssh sur node2

PS C:\WINDOWS\system32> ssh david@10.2.1.12
    
    david@10.2.1.12's password:
    Activate the web console with: systemctl enable --now cockpit.socket
    Last login: Mon Sep 20 16:17:07 2021 from 10.2.1.1
    [david@node2 ~]$
    
### Ping d'une machine à l'autre et observation les tables ARP des deux machines

Node1

Ping vers Node2

        [david@node1 ~]$ ping 10.2.1.12
        PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
        64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.803 ms
        64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=1.02 ms
        
        
        
Table arp:

        [david@node1 ~]$ ip n s
        10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:16 REACHABLE
        10.2.1.12 dev enp0s8 lladdr 08:00:27:07:c9:94 STALE
        
        Adresse MAC de Node2 associée à l'Ip 10.2.1.12:
                    "08:00:27:07:c9:94"

Node2

Ping vers Node1

        [david@node2 ~]$ ping 10.2.1.12
        PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
        64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.077 ms
        64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.081 ms
        
Table arp:

    [david@node2 ~]$ ip n s
    10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:16 REACHABLE
    10.2.1.11 dev enp0s8 lladdr 08:00:27:45:bf:3f STALE
        
        Adresse MAC de Node1 associée à l'Ip 10.2.1.11:
                    "08:00:27:45:bf:3f"
                    
Confirmation de l'adresse MAC de Node1:

    [david@node1 ~]$ ip a
        [...]
        3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
            link/ether     08:00:27:45:bf:3f      brd ff:ff:ff:ff:ff:ff
        [...]         
    
Confirmation de l'adresse MAC de Node2:

    [david@node1 ~]$ ip a
        [...]
        3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether         08:00:27:07:c9:94     brd ff:ff:ff:ff:ff:ff
        [...]  
                     

# 2. Analyse de trames

## Commande tcpdump pour réaliser une capture de trame

Sur node 1:

    [david@node1 ~]$ sudo tcpdump -i enp0s8 -w tp2_arp.pcap not port 22
    dropped privs to tcpdump
    tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes

Flush de la table ARP Node 1

    [david@node1 ~]$ sudo ip -s -s neigh flush all
    10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:16 ref 1 used 21/0/18 probes 4 REACHABLE
    10.2.1.12 dev enp0s8 lladdr 08:00:27:07:c9:94 ref 1 used 13/13/13 probes 4 REACHABLE

    *** Round 1, deleting 2 entries ***
    *** Flush is complete after 1 round ***
    
    
    
    [david@node1 ~]$ ip n s
    10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:16 DELAY
    [david@node1 ~]$
    
Ping de Node2: 

    [david@node1 ~]$ ping 10.2.1.12
    PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
    64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=1.62 ms
    ^C
    --- 10.2.1.12 ping statistics ---
    1 packets transmitted, 1 received, 0% packet loss, time 0ms
    rtt min/avg/max/mdev = 1.622/1.622/1.622/0.000 ms
    
Flush de la table ARP Node 2:

    [david@node2 ~]$ sudo ip -s -s neigh flush all
    [sudo] password for david:
    10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:16 ref 1 used 22/0/22 probes 1 REACHABLE
    10.2.1.11 dev enp0s8 lladdr 08:00:27:45:bf:3f used 83/81/39 probes 1 STALE

    *** Round 1, deleting 2 entries ***
    *** Flush is complete after 1 round ***
    
Ping de Node1: 
    
    [david@node2 ~]$ ping 10.2.1.11
    PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
    64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.591 ms
    ^C
    --- 10.2.1.11 ping statistics ---
    1 packets transmitted, 1 received, 0% packet loss, time 0ms
    rtt min/avg/max/mdev = 0.591/0.591/0.591/0.000 ms
    

Résultat de tcpdump sur Node1:
    
    ^C8 packets captured
    8 packets received by filter
    0 packets dropped by kernel
    
    
### Tram arp

| ordre | type trame  | source                   | destination                |
|-------|-------------|--------------------------|----------------------------|
| 1     | Request ARP | `node1` `08:00:27:45:bf:3f` | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Reply ARP   | `node2` `08:00:27:07:c9:94` | `node1` `08:00:27:45:bf:3f`   |
| 3     | Request ICMP| `node1` `08:00:27:45:bf:3f` | `node2` `08:00:27:07:c9:94`   |
| 4     | Reply ICMP  | `node2` `08:00:27:07:c9:94` | `node1` `08:00:27:07:c9:94`   |
| 5     | Request ICMP| `node2` `08:00:27:07:c9:94` | `node1` `08:00:27:45:bf:3f`   |
| 6     | Reply ICMP  | `node1` `08:00:27:45:bf:3f` | `node2` `08:00:27:07:c9:94`   |
| 7     | Request ARP | `node2` `08:00:27:07:c9:94` | `node1` `08:00:27:45:bf:3f`   |
| 8     | Reply ARP   | `node1` `08:00:27:45:bf:3f` | `node2` `08:00:27:07:c9:94`   |


# II. Routage

## 1. Mise en place du routage

### Activer le routage sur le noeud router.net2.tp2

Il faut d'abord être root sur notre hôte:

    [david@router ~]$ su -
    Password:
    Last login: Thu Sep 23 14:35:49 CEST 2021 on pts/0
    
Configuration persistant du mode routage:

    [root@router ~]# echo "net.ipv4.ip_forward=1" > /etc/sysctl.d/routeur.conf
    
Prise en compte de notre configuration:

    [root@router ~]# sysctl -p /etc/sysctl.d/routeur.conf
    net.ipv4.ip_forward = 1
 
Ajout des routes statiques:

Node1:
    
    [david@node1 ~]$ vim /etc/sysconfig/network-scripts/ifcfg-enp0s8
    [...]
    GATEWAY=10.2.1.254
    [david@marcel ~]$ sudo nmcli connection reload
    [david@node1 ~]$ sudo nmcli connection up enp0s8
    Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/3)

Marcel:

    [david@marcel ~]$ sudo vim /etc/sysconfig/network-scripts/ifcfg-enp0s8
    [...]
    GATEWAY=10.2.2.254
    [david@marcel ~]$ sudo nmcli connection reload
    [david@marcel ~]$ sudo nmcli connection up enp0s8
    Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/3)

    
Vérifivations du routage:

   Ping de node1 vers Marcel:
   
    [david@node1 ~]$ ping 10.2.2.12
    PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
    64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.23 ms
    
  Ping de Marcel vers Node1:
  
    [david@marcel ~]$ ping 10.2.1.11
    PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
    64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=1.63 ms
    
## 2. Analyse de trames

    [david@node1 ~]$ ping 10.2.2.12
    PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
    64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.96 ms
    

TRAM du fichier tp2_routage_node1.pcap:

    [david@node1 ~]$sudo tcpdump -i enp0s8 not port 22 -w tp2_routage_node1.pcap    
    
| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP | 10.2.1.11 | `node1` `08:00:27:45:bf:3f`  | 10.2.1.154  | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | 10.2.1.154| `router` `08:00:27:65:39:55` | 10.2.1.11   | `node1` `08:00:27:45:bf:3f`|
| 3     | Ping        | 10.2.1.11 | `node1` `08:00:27:45:bf:3f`  | 10.2.2.12   | `router``08:00:27:65:39:55`|
| 4     | Pong        | 10.2.2.12 | `router` `08:00:27:65:39:55` | 10.2.1.11   | `node1` `08:00:27:45:bf:3f`|
| 5     | Requête ARP | 10.2.1.154| `router` `08:00:27:65:39:55` | 10.2.1.11   | `node1` `08:00:27:45:bf:3f`|
| 6     | Réponse ARP | 10.2.1.11 | `node1` `08:00:27:45:bf:3f`  | 10.2.1.254  | `router``08:00:27:65:39:55`| 


TRAM du fichier tp2_routage_router.pcap:

    [david@router ~]$sudo tcpdump -i enp0s8 not port 22 -w tp2_routage_router.pcap    
    

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP | 10.2.1.11 | `node1` `08:00:27:45:bf:3f`  | 10.2.1.154  | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | 10.2.1.154| `router` `08:00:27:65:39:55` | 10.2.1.11   | `node1` `08:00:27:45:bf:3f`|
| 3     | Ping        | 10.2.1.11 | `node1` `08:00:27:45:bf:3f`  | 10.2.2.12   | `router``08:00:27:65:39:55`|
| 4     | Pong        | 10.2.2.12 | `router` `08:00:27:65:39:55` | 10.2.1.11   | `node1` `08:00:27:45:bf:3f`|
| 5     | Requête ARP | 10.2.1.154| `router` `08:00:27:65:39:55` | 10.2.1.11   | `node1` `08:00:27:45:bf:3f`|
| 6     | Réponse ARP | 10.2.1.11 | `node1` `08:00:27:45:bf:3f`  | 10.2.1.254  | `router``08:00:27:65:39:55`| 

TRAM du fichier tp2_routage_marcel.pcap:

    [david@marcel ~]$sudo tcpdump -i enp0s8 not port 22 -w tp2_routage_marcel.pcap 

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP | 10.2.2.254| `marcel` `08:00:27:9d:69:47`  | 10.2.1.154  | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | 10.2.2.12| `router` `08:00:27:1c:f2:31` | 10.2.2.254   | `marcel` `08:00:27:9d:69:47`|
| 3     | Ping        | 10.2.1.11 | `marcel` `08:00:27:9d:69:47`  | 10.2.2.12   | `router` `08:00:27:1c:f2:31`|
| 4     | Pong        | 10.2.2.12 | `router` `08:00:27:1c:f2:31` | 10.2.1.11   | `marcel` `08:00:27:9d:69:47`|
| 5     | Requête ARP | 10.2.2.12| `router` `08:00:27:1c:f2:31` | 10.2.2.254   | `marcel` `08:00:27:9d:69:47`|
| 6     | Réponse ARP | 10.2.2.254 | `marcel` `08:00:27:9d:69:47`  | 10.2.2.12  | `router` `08:00:27:1c:f2:31`| 

## 3. Accès internet


### Ajout des routes par défaut à node1.net1.tp2 et marcel.net2.tp2

    Les routes par defauts ont été configurées plus précéedemment dans le fichier "/etc/sysconfig/network-scripts/ifcfg-enp0s8"


Activation du NAT:

    [[david@router ~]$ sudo firewall-cmd --add-masquerade --per
    success
    [david@router ~]$ sudo firewall-cmd --reload
    success
    
Ping de Marcel vers 8.8.8.8:

    [david@marcel ~]$ ping 8.8.8.8
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=20.6 ms
    ^C
    --- 8.8.8.8 ping statistics ---
    1 packets transmitted, 1 received, 0% packet loss, time 0ms
    
Ping de Node1 vers 8.8.8.8:

    [david@node1 ~]$ ping 8.8.8.8
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=21.2 ms
    64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=20.5 ms
    ^C
    --- 8.8.8.8 ping statistics ---
    2 packets transmitted, 2 received, 0% packet loss, time 1001ms
    rtt min/avg/max/mdev = 20.491/20.850/21.209/0.359 ms
    
Adresse DNS:

Marcel:
    
    [david@marcel ~]$ sudo nmcli connection modify enp0s8 ipv4.dns 1.1.1.1
    [david@marcel ~]$ sudo nmcli connection reload
    [david@marcel ~]$ sudo nmcli connection up enp0s8
    Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/4)

  Vérifications:
  
    [david@marcel ~]$ dig google.com
    ;; QUESTION SECTION:
    ;google.com.                    IN      A
    [...]
    ;; ANSWER SECTION:
    google.com.             190     IN      A       216.58.214.78
    [...]
    ;; SERVER: 1.1.1.1#53(1.1.1.1)
    [...]

  Node1:
    
    [david@node1 ~]$ sudo nmcli connection modify enp0s8 ipv4.dns 1.1.1.1
    [david@node1 ~]$ sudo nmcli connection reload
    [david@marcel ~]$ sudo nmcli connection up enp0s8
    Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/4)
 
    
  Vérifications:
  
      [david@node1 ~]$ dig google.com
    [...]
    ;; QUESTION SECTION:
    ;google.com.                    IN      A
    ;; ANSWER SECTION:
    google.com.             50      IN      A       142.250.75.238
    [...]
    ;; SERVER: 1.1.1.1#53(1.1.1.1) 
    [...]
    
### Analyse de trames 
    
    [david@node1 ~]$sudo tcpdump -i enp0s8 not port 22 -w tp2_routage_internet.pcap
    [david@node1 ~]$ ping 8.8.8.8
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=27.8 ms
    ^C
    --- 8.8.8.8 ping statistics ---
    1 packets transmitted, 1 received, 0% packet loss, time 0ms
    rtt min/avg/max/mdev = 27.819/27.819/27.819/0.000 ms  
    
    
| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Ping        | 10.2.1.11 | `node1` `08:00:27:45:bf:3f`  | 8.8.8.8     | `router` `08:00:27:65:39:55` |
| 2     | Pong        | 8.8.8.8   | `router` `08:00:27:65:39:55` | 10.2.1.11   | `node1` `08:00:27:45:bf:3f`|
| 3     | Requête ARP | 10.2.1.254 | `router` `08:00:27:65:39:55`  | 10.2.1.11   | `node1` `08:00:27:45:bf:3f`|
| 4     | Réponse ARP | 10.2.1.11 | `node1` `08:00:27:45:bf:3f` | 10.2.1.254   | `router` `08:00:27:65:39:55`|
| 5     | Requête ARP | 10.2.1.11 | `node1` `08:00:27:45:bf:3f` | 10.2.1.254   | `router` `08:00:27:65:39:55`|
| 6     | Réponse ARP | 10.2.1.254 | `router` `08:00:27:65:39:55`  | 10.2.1.11  | `node1` `08:00:27:45:bf:3f`| 


# III. DHCP

  Installation de dhcp-server et configuation:

    [david@node1 ~]$ sudo dnf install dhcp-server
    
    #
    # DHCP Server Configuration file.
    #   see /usr/share/doc/dhcp-server/dhcpd.conf.example
    #   see dhcpd.conf(5) man page

    # specify DNS server's hostname or IP address
    option domain-name-servers     1.1.1.1;
    # default lease time
    default-lease-time 600;
    # max lease time
    max-lease-time 7200;
    # this DHCP server to be declared valid
    authoritative;
    # specify network address and subnetmask
    subnet 10.2.1.0 netmask 255.255.255.0 {
        # specify the range of lease IP address
        range dynamic-bootp 10.2.1.20 10.2.1.200;
        # specify broadcast address
        option broadcast-address 10.2.1.255;
        # specify gateway
        option routers 10.2.1.254;
    }
    
    [david@node1 ~]$ sudo  systemctl start dhcpd.service
    [david@node1 ~]$ sudo systemctl enable dhcpd.service
    
  Ouverture du firewall:

    [david@node1 ~]$ sudo firewall-cmd --add-service=dhcp --permanent
    success
    [david@node1 ~]$ sudo firewall-cmd --reload
    success
    
client dhcp:

  vérifier avec une commande qu'il a récupéré son IP:
    
    [david@node2 ~]$ ip a | grep dynamic
    inet 10.2.1.21/24 brd 10.2.1.255 scope global secondary dynamic enp0s8

Ping asserelle

    [david@node2 ~]$ ping 10.2.1.254
    PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
    64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=1.15 ms

Présence de la route 

    [david@node2 ~]$ ip r s
    default via 10.2.1.254 dev enp0s8

Route fonctionnelle 

    [david@node2 ~]$ ping 10.2.2.12
    PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
    64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.45 ms



Vérification de la commande dig 

    [david@node2 ~]$ dig google.com
    ;; QUESTION SECTION:
    ;google.com.                    IN      A

    ;; ANSWER SECTION:
    google.com.             243     IN      A       216.58.214.78

    ;; Query time: 30 msec
    ;; SERVER: 1.1.1.1#53(1.1.1.1)
    [...]

Ping nom de domaine
    
    [david@node2 ~]$ ping google.com
    PING google.com (172.217.19.238) 56(84) bytes of data.
    64 bytes from par21s11-in-f14.1e100.net (172.217.19.238): icmp_seq=1 ttl=113 time=39.8 ms
    ^C
    --- google.com ping statistics ---
    2 packets transmitted, 1 received, 50% packet loss, time 1001ms
    rtt min/avg/max/mdev = 39.811/39.811/39.811/0.000 ms

## 2. Analyse de trames
    
Vider les tables arp:

node1:

    [david@node1 ~]$ sudo ip -s -s neigh flush all
    10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:16 ref 1 used 6/0/6 probes 4 REACHABLE

    *** Round 1, deleting 1 entries ***
    *** Flush is complete after 1 round ***
  
node2:

    [david@node2 ~]$ sudo ip -s -s neigh flush all
    [sudo] password for david:
    10.2.1.11 dev enp0s8 lladdr 08:00:27:45:bf:3f ref 1 used 33/33/33 probes 4 REACHABLE
    10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:16 ref 1 used 0/0/0 probes 0 REACHABLE
    10.2.1.254 dev enp0s8 lladdr 08:00:27:65:39:55 used 107/105/86 probes 1 STALE

    *** Round 1, deleting 3 entries ***
    *** Flush is complete after 1 round ***
    
Capture des pacquets depuis le server dhcp:

    [david@node1 ~]$ sudo tcpdump -i enp0s8 not port 22 -w tp2_dhcp.pcap
    
Renouvellement de bail et demande d'une nouvelle IP:

    [david@node2 ~]$ sudo dhclient -r
    [david@node2 ~]$ sudo dhclient 
    
Analyse des tram:

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP | 10.2.1.22 | `node2` `08:00:27:07:c9:94` | 10.2.1.11    | Broadcast `FF:FF:FF:FF:FF`|
| 2     | Réponse ARP | 10.2.1.11 | `node1` `08:00:27:45:bf:3f` | 10.2.1.22    | `node2` `08:00:27:07:c9:94`  |
| 3     | DHCP release| 10.2.1.22 | `node2` `08:00:27:07:c9:94` | 10.2.1.11    | `node1` `08:00:27:45:bf:3f`|
| 4     | DHCP discover | 0.0.0.0 | `node2` `08:00:27:07:c9:94` | 255.255.255.255   | Broadcast `FF:FF:FF:FF:FF`|
| 5     | DHCP offer  | 10.2.1.11 | `node1` `08:00:27:45:bf:3f` | 10.2.1.22   | `node2` `08:00:27:07:c9:94`|
| 6     | DHCP request | 0.0.0.0 | `node2` `08:00:27:07:c9:94`  | 255.255.255.255  | Broadcast `FF:FF:FF:FF:FF`| 
| 7     | DHCP ACK    | 10.2.1.11 | `node1` `08:00:27:45:bf:3f` | 10.2.1.22   | `node2` `08:00:27:07:c9:94`|
| 8     | Requête ARP  | 0.0.0.0 | `node2` `08:00:27:07:c9:94` | 10.2.1.22   | Broadcast `FF:FF:FF:FF:FF`|
| 9     | Requête ARP  | 0.0.0.0 | `node2` `08:00:27:07:c9:94` | 10.2.1.22   | Broadcast `FF:FF:FF:FF:FF`| 
| 10     | Requête ARP  | 10.2.1.11 | `node1` `08:00:27:45:bf:3f` | 10.2.1.22   | `node2` `08:00:27:07:c9:94`|
| 11     | Réponse ARP  | 10.2.1.22 | `node2` `08:00:27:07:c9:94` | 10.2.1.11  | `node1` `08:00:27:45:bf:3f`| 